# Validate a UK Phone Number with RegEX
This RegEx pattern will allow for Validated UK Phone Numbers of 10 or 11 digits, optionally including +44 prefix as well as 3 or 4 digits as an extension number

## Examples
Matches: +447222111111 | +44 7222 111 111 | +44 7222 111111 | 07222 111 111 | 07222 111 111 #222 | +447222 111 111 #222 | (+447222)111111

Non-Matches: +44(7222)555555 | 88222 111 111

## RegEx breakdown:
Non-capturing group (?:(?:\(?(?:0(?:0|11)\)?[\s-]?\(?|\+)44\)?[\s-]?(?:\(?0\)?[\s-]?)?)|(?:\(?0))



__1st Alternative__ (?:\(?(?:0(?:0|11)\)?[\s-]?\(?|\+)44\)?[\s-]?(?:\(?0\)?[\s-]?)?)

__Non-capturing group__ (?:\(?(?:0(?:0|11)\)?[\s-]?\(?|\+)44\)?[\s-]?(?:\(?0\)?[\s-]?)?)

\(? matches the character ( literally (case sensitive)

? Quantifier — Matches between zero and one times, as many times as possible, giving back as needed (greedy)

__Non-capturing group__ (?:0(?:0|11)\)?[\s-]?\(?|\+)

__1st Alternative__ 0(?:0|11)\)?[\s-]?\(?

0 matches the character 0 literally (case sensitive)

Non-capturing group (?:0|11)

\)? matches the character ) literally (case sensitive)

Match a single character present in the list below [\s-]?

\(? matches the character ( literally (case sensitive)



__2nd Alternative__ \+

\+ matches the character + literally (case sensitive)

44 matches the characters 44 literally (case sensitive)

\)? matches the character ) literally (case sensitive)

? Quantifier — Matches between zero and one times, as many times as possible, giving back as needed (greedy)

Match a single character present in the list below [\s-]?




__Non-capturing group__ (?:\(?0\)?[\s-]?)?


__2nd Alternative__ (?:\(?0)

Non-capturing group (?:\(?0)

\(? matches the character ( literally (case sensitive)

? Quantifier — Matches between zero and one times, as many times as possible, giving back as needed (greedy)

0 matches the character 0 literally (case sensitive)

__Non-capturing group__ (?:(?:\d{5}\)?[\s-]?\d{4,5})|(?:\d{4}\)?[\s-]?(?:\d{5}|\d{3}[\s-]?\d{3}))|(?:\d{3}\)?[\s-]?\d{3}[\s-]?\d{3,4})|(?:\d{2}\)?[\s-]?\d{4}[\s-]?\d{4}))

__1st Alternative__ (?:\d{5}\)?[\s-]?\d{4,5})

Non-capturing group (?:\d{5}\)?[\s-]?\d{4,5})

\d{5} matches a digit (equal to [0-9])

{5} Quantifier — Matches exactly 5 times

\)? matches the character ) literally (case sensitive)

? Quantifier — Matches between zero and one times, as many times as possible, giving back as needed (greedy)

Match a single character present in the list below [\s-]?

\? Quantifier — Matches between zero and one times, as many times as possible, giving back as needed (greedy)
\s matches any whitespace character (equal to [\r\n\t\f\v ])

\- matches the character - literally (case sensitive)
\d{4,5} matches a digit (equal to [0-9])

__2nd Alternative__ (?:\d{4}\)?[\s-]?(?:\d{5}|\d{3}[\s-]?\d{3}))
Non-capturing group (?:\d{4}\)?[\s-]?(?:\d{5}|\d{3}[\s-]?\d{3}))
\d{4} matches a digit (equal to [0-9])
{4} Quantifier — Matches exactly 4 times
\)? matches the character ) literally (case sensitive)
? Quantifier — Matches between zero and one times, as many times as possible, giving back as needed (greedy)
Match a single character present in the list below [\s-]?
Non-capturing group (?:\d{5}|\d{3}[\s-]?\d{3})

__3rd Alternative__ (?:\d{3}\)?[\s-]?\d{3}[\s-]?\d{3,4})
Non-capturing group (?:\d{3}\)?[\s-]?\d{3}[\s-]?\d{3,4})
\d{3} matches a digit (equal to [0-9])
{3} Quantifier — Matches exactly 3 times
\)? matches the character ) literally (case sensitive)
Match a single character present in the list below [\s-]?
\d{3} matches a digit (equal to [0-9])
Match a single character present in the list below [\s-]?
\d{3,4} matches a digit (equal to [0-9])
4th Alternative (?:\d{2}\)?[\s-]?\d{4}[\s-]?\d{4})
Non-capturing group (?:\d{2}\)?[\s-]?\d{4}[\s-]?\d{4})
\d{2} matches a digit (equal to [0-9])
\)? matches the character ) literally (case sensitive)
Match a single character present in the list below [\s-]?
\d{4} matches a digit (equal to [0-9])
Match a single character present in the list below [\s-]?
\d{4} matches a digit (equal to [0-9])
Non-capturing group (?:[\s-]?(?:x|ext\.?|\#)\d{3,4})?
? Quantifier — Matches between zero and one times, as many times as possible, giving back as needed (greedy)
Match a single character present in the list below [\s-]?
? Quantifier — Matches between zero and one times, as many times as possible, giving back as needed (greedy)
\s matches any whitespace character (equal to [\r\n\t\f\v ])
- matches the character - literally (case sensitive)

__Non-capturing group__ (?:x|ext\.?|\#)
__1st Alternative__ x
x matches the character x literally (case sensitive)

__2nd Alternative__ ext\.?

__3rd Alternative__ \#
\d{3,4} matches a digit (equal to [0-9])
{3,4} Quantifier — Matches between 3 and 4 times, as many times as possible, giving back as needed (greedy)
$ asserts position at the end of the string, or before the line terminator right at the end of the string (if any)

## Author
Joe Hatch
Inspire Digital 
joe@inspiredigital.co